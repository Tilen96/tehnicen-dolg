var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var DOlg = require('../models/Dolg.js');

/* GET ALL DOLGOVI */
router.get('/', function(req, res, next) {
  DOlg.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* GET SINGLE DOLG BY ID */
router.get('/:id', function(req, res, next) {
  DOlg.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* SAVE DOLG */
router.post('/', function(req, res, next) {
  DOlg.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* UPDATE DOLG */
router.put('/:id', function(req, res, next) {
  DOlg.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE DOLG */
router.delete('/:id', function(req, res, next) {
  DOlg.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
