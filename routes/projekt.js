var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Projekt = require('../models/Projekt.js');

var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'tehnicendolg@gmail.com',
    pass: 'TilenHlis2821996'
  }
});

/* GET ALL PROJEKTI */
router.get('/', function(req, res, next) {
  Projekt.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* GET SINGLE PROJEKT BY ID */
router.get('/:id', function(req, res, next) {
  Projekt.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* SAVE PROJEKT */
router.post('/', function(req, res, next) {
  Projekt.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);

    const mailOptions = {
      from: 'tehnicendolg@email.com', // sender address
      to: post['clan']+', '+post['clan1']+','+post['clan2']+','+post['clan3']+','+post['clan4']+','+post['clan5']+','+post['clan6']+','+post['clan7']+','+post['clan8']+','+post['clan9'], // list of receivers
      subject: 'Platforma tehničen dolg', // Subject line
      html: '<p>Pozdravljeni!<br><br>Dodani ste bili v projekt '+post['naziv']+' na spletni platformi za upravljanje tehničnega dolga.' +
      '<br>Registrirajte se z elektronskim naslovom na katerega ste prejeli to sporočilo.</p>'// plain text body
    };

    transporter.sendMail(mailOptions, function (err, info) {
      if(err)
        console.log(err)
      else
        console.log(info);
    });

  });
});

/* UPDATE PROJEKT */
router.put('/:id', function(req, res, next) {
  Projekt.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE PROJEKT */
router.delete('/:id', function(req, res, next) {
  Projekt.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
