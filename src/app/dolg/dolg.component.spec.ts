import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DolgComponent } from './dolg.component';

describe('DolgComponent', () => {
  let component: DolgComponent;
  let fixture: ComponentFixture<DolgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DolgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DolgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
