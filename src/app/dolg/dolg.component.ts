import {Component, OnInit, ViewChild} from '@angular/core';
import { ApiService } from '../api.service';
import { DataSource } from '@angular/cdk/collections';
import {MatSort, MatTableDataSource} from '@angular/material';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-dolg',
  templateUrl: './dolg.component.html',
  styleUrls: ['./dolg.component.css']
})
export class DolgComponent implements OnInit {

  displayedColumns = ['naziv', 'datum', 'kriticnost', 'status'];
  dataSource: any;

  constructor(private api: ApiService, public auth: AuthService) { }

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.api.getDolgovi()
      .subscribe(res => {
        this.auth.getProfile((err, profile) => {
          let podatki = [];
          for (let i in res) {
            podatki = podatki.concat([res[i]]);
          }
          let email: string;
          this.dataSource = new MatTableDataSource(podatki);
          email = profile.email.trim();
          email = email.toLowerCase();
          this.dataSource.filter = email;
          this.dataSource.sort = this.sort;
        });
      }, err => {
        console.log(err);
      });
  }
}
