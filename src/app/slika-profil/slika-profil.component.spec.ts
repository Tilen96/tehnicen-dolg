import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlikaProfilComponent } from './slika-profil.component';

describe('SlikaProfilComponent', () => {
  let component: SlikaProfilComponent;
  let fixture: ComponentFixture<SlikaProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlikaProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlikaProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
