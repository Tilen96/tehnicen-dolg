import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjektPodrobnostiComponent } from './projekt-podrobnosti.component';

describe('ProjektPodrobnostiComponent', () => {
  let component: ProjektPodrobnostiComponent;
  let fixture: ComponentFixture<ProjektPodrobnostiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjektPodrobnostiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjektPodrobnostiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
