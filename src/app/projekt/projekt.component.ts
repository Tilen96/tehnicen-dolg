import {Component, OnInit, ViewChild} from '@angular/core';
import { ApiService } from '../api.service';
import {MatSort, MatTableDataSource} from '@angular/material';
import {AuthService} from '../auth/auth.service';


@Component({
  selector: 'app-projekt',
  templateUrl: './projekt.component.html',
  styleUrls: ['./projekt.component.css']
})
export class ProjektComponent implements OnInit {

  displayedColumns = ['naziv', 'datum', 'alert'];
  dataSource: any;

  constructor(private api: ApiService, public auth: AuthService) { }

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
   this.api.getProjekti()
      .subscribe(res => {
          this.auth.getProfile((err, profile) => {
            let podatki = [];
            for (let i in res) {
              podatki = podatki.concat([res[i]]);
            }
            let email: string;
            this.dataSource = new MatTableDataSource(podatki);
            email = profile.email.trim();
            email = email.toLowerCase();
            this.dataSource.filter = email;
            this.dataSource.sort = this.sort;
          });
        }, err => {
        console.log(err);
      });
  }
}
