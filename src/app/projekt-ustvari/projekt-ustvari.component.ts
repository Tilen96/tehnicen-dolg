import {Component, Inject, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-projekt-ustvari',
  templateUrl: './projekt-ustvari.component.html',
  styleUrls: ['./projekt-ustvari.component.css']
})
export class ProjektUstvariComponent implements OnInit{

  profile: any;
  vloga: string;

  constructor(public dialog: MatDialog, public auth: AuthService) { }

  ngOnInit() {
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
        this.vloga = this.auth._checkAdmin(this.auth.userProfile)[0];
      });
    }
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(UstavriProjektDialogOverviewDialogComponent, {
      width: '750px',
      height: '600px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}

@Component({
  selector: 'app-projekt-ustvari-dialog',
  templateUrl: './projekt-ustvari.dialog.component.html',
  styleUrls: ['./projekt-ustvari.component.css']
})
export class UstavriProjektDialogOverviewDialogComponent implements OnInit {

  projektForm: FormGroup;
  naziv = '';
  clan = '';
  clan1 = '';
  clan2 = '';
  clan3 = '';
  clan4 = '';
  clan5 = '';
  clan6 = '';
  clan7 = '';
  clan8 = '';
  clan9 = '';
  tip_ocena = '';
  zgornja_meja_dolga = '';
  stevec = 0;
  stevec1 = false;
  stevec2 = false;
  stevec3 = false;
  stevec4 = false;
  stevec5 = false;
  stevec6 = false;
  stevec7 = false;
  stevec8 = false;
  stevec9 = false;
  eur = false;
  dnevi = false;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<UstavriProjektDialogOverviewDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.projektForm = this.formBuilder.group({
      'naziv' : [null, Validators.required],
      'clan' : [null, Validators.required],
      'clan1' : [null],
      'clan2' : [null],
      'clan3' : [null],
      'clan4' : [null],
      'clan5' : [null],
      'clan6' : [null],
      'clan7' : [null],
      'clan8' : [null],
      'clan9' : [null],
      'tip_ocena' : [null, Validators.required],
      'zgornja_meja_dolga' : [null, Validators.required]
    });
  }

  onFormSubmit(form: NgForm) {
    this.api.postProjekt(form)
      .subscribe(res => {
        const id = res['_id'];
        this.router.navigate(['/', { outlets: {primary: ['projekt-podrobnosti', id ], dolg: ['dolgovi-od-projekta'] } }]);
        this.dialogRef.close();
      }, (err) => {
        console.log(err);
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  dodajClana(): void {
    this.stevec = this.stevec + 1;
    if (this.stevec === 1) {
      this.stevec1 = true;
    } else if (this.stevec === 2) {
      this.stevec2 = true;
    } else if (this.stevec === 3) {
      this.stevec3 = true;
    } else if (this.stevec === 4) {
      this.stevec4 = true;
    } else if (this.stevec === 5) {
      this.stevec5 = true;
    } else if (this.stevec === 6) {
      this.stevec6 = true;
    } else if (this.stevec === 7) {
      this.stevec7 = true;
    } else if (this.stevec === 8) {
      this.stevec8 = true;
    } else if (this.stevec === 9) {
      this.stevec9 = true;
    }
  }

  eur_izbira(): void {
    this.eur = true;
    this.dnevi = false;
  }

  dnevi_izbira(): void {
    this.dnevi = true;
    this.eur = false;
  }

}
