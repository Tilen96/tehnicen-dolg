import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DolgUrediComponent } from './dolg-uredi.component';

describe('DolgUrediComponent', () => {
  let component: DolgUrediComponent;
  let fixture: ComponentFixture<DolgUrediComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DolgUrediComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DolgUrediComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
