import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjektUrediComponent } from './projekt-uredi.component';

describe('ProjektUrediComponent', () => {
  let component: ProjektUrediComponent;
  let fixture: ComponentFixture<ProjektUrediComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjektUrediComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjektUrediComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
