import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  profile: any;

  constructor(public auth: AuthService, public dialog: MatDialog, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
      });
    }
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(DialogOverviewDialogComponent, {
      width: '250px',
      height: '300px',
      data: { name: this.profile.name, picture: this.sanitizer.bypassSecurityTrustResourceUrl(this.profile.picture), email: this.profile.email }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

@Component({
  selector: 'app-profil-dialog',
  templateUrl: './profil.dialog.component.html',
  styleUrls: ['./profil.component.css']
})
export class DialogOverviewDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
