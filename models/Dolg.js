var mongoose = require('mongoose');

var DolgSchema = new mongoose.Schema({
  naziv: String,
  vzrok: String,
  tip: String,
  podtip: String,
  opis: String,
  obseg: String,
  ocena: String,
  kriticnost: String,
  status: String,
  datum: { type: Date, default: Date.now },
  id_projekt: String,
  clan: String,
  clan1: String,
  clan2: String,
  clan3: String,
  clan4: String,
  clan5: String,
  clan6: String,
  clan7: String,
  clan8: String,
  clan9: String,
});

module.exports = mongoose.model('Dolg', DolgSchema);
